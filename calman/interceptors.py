from django.http import HttpResponse
from django.conf import settings

import urllib
import urllib.request
import urllib.error
import time
import json

def update_creds(get_response):
    def new_access_token(request):
        if 'username' in request.session:
            now = time.time()

            expired = now >= request.session['expires_in']
            expired = expired or ((request.session['expires_in'] - now) < 300)

            if expired:
                options = {
                    'client_id': settings.GOOGLE_CLIENT_ID,
                    'client_secret': settings.GOOGLE_CLIENT_SECRET,
                    'refresh_token': request.session['refresh_token'],
                    'grant_type': 'refresh_token'
                }

                try:
                    with urllib.request.urlopen(
                        'https://www.googleapis.com/oauth2/v4/token',
                        data=urllib.parse.urlencode(options).encode()) as res:
                            creds = json.loads(res.read().decode())
                except urllib.error.URLError as e:
                    return HttpResponse(e.reason, status=e.code)

                request.session['access_token'] = creds['access_token']
                request.session['expires_in'] = time.time() + creds['expires_in']

        response = get_response(request)

        return response

    return new_access_token
