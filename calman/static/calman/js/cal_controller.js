var app = angular.module('emails', [])

app.config(function($httpProvider) {
    $httpProvider.defaults.xsrfCookieName = 'csrftoken'
    $httpProvider.defaults.xsrfHeaderName = 'X-CSRFToken'
})

app.controller('calCtrl', function($scope, $http, $compile) {
    $scope.showEventList = true
    $scope.showAddEvent = false

    $scope.checkRemEvent = false
    $scope.submitRem = false
    $scope.noRem = false

    function submitCancel(on) {
        $scope.submitRem = on
        $scope.noRem = on
    }

    $scope.showAddPanel = function() {
        $scope.showEventList = false
        $scope.showAddEvent = true

        submitCancel(false)
    }

    $scope.closeAddPanel = function() {
        $scope.showEventList = true
        $scope.showAddEvent = false

        if($('.panel-group').find('input[type=checkbox]').length > 0)
            submitCancel(true)
    }

    $scope.addEvent = function() {
        var start = $scope.startDate.split('-').map(function(x) {
            return Number(x)
        })

        var end = $scope.endDate.split('-').map(function(x) {
            return Number(x)
        })

        if(start[0] > end[0] || start[1] > end[1] || start[2] > end[2])
            return alert('Start date can not be after end date')

        var today = new Date()

        if(start[0] < today.getFullYear() || start[1] < today.getMonth() || start[2] < today.getDate())
            return alert('Add an event from this day onward.')

        $http.post('events/add/', {
            title: $scope.title,
            description: $scope.description,
            start: $scope.startDate,
            end: $scope.endDate
        }).then(function success(res) {
            var newElm = angular.element(res.data)
            $compile(newElm)($scope)

            $('.panel-group').prepend(newElm)

            alert('Event added')
        }, function failure(res) {
            alert('Failed to add event')
        })
    }

    $scope.removeEvents = function() {
        $scope.closeAddPanel()

        if($('.panel-group').children('.panel').length == 0) {
            $scope.checkRemEvent = false

            submitCancel(false)
        } else {
            $scope.checkRemEvent = true

            submitCancel(true)
        }
    }

    $scope.reqRem = function() {
        var toRem = []

        $("input[ng-if=checkRemEvent]:checked").each(function(index, value) {
            toRem.push($(value).parent().siblings()['0'].id)
        })

        if(toRem.length == 0)
            return alert('Select an email to remove')

        $http.post('events/remove/', {
            events: toRem
        }).then(function success(res) {
            toRem.forEach(function(item) {
                $('[id=' + item + ']').parents('.panel').remove()
            })

            if($('.panel-group').children('.panel').length == 0)
                submitCancel(false)
        }, function failure(res) {
            alert(res.data)
        })
    }

    $scope.cancelRem = function() {
        $scope.checkRemEvent = false
        $scope.submitRem = false
        $scope.noRem = false

        $("input[ng-if=checkRemEvent]:checked").each(function(index, value) {
            value.checked = false
        })
    }
})
