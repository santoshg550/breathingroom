from django.shortcuts import render, redirect
from django.http import HttpResponse
from django.conf import settings
from django.views.decorators.csrf import ensure_csrf_cookie

import urllib
import urllib.error
import urllib.request
import os
import base64
import time
import datetime
import json

from .models import Users, Events

def prep_res(events):
    res = []

    for event in events:
        res.append({
            'event_id': event.event_id,
            'title': event.title,
            'description': event.description,
            'start': event.start,
            'end': event.end
        })

    return { 'events': res }

@ensure_csrf_cookie
def home(request):
    if 'username' not in request.session:
        return render(request, 'calman/login.html')

    try:
        events = Events.objects.filter(username=request.session['username'])
    except Events.DoesNotExist:
        events = []

    return render(request, 'calman/home.html', prep_res(events))

def login(request):
    if 'username' in request.session:
        return render(request, 'calman/home.html')

    scopes = [
        'email',
        'profile',
        'https://www.googleapis.com/auth/calendar',
        'https://www.googleapis.com/auth/calendar.events'
    ]

    state = base64.urlsafe_b64encode(os.urandom(32)).decode()

    request.session['state'] = state

    options = urllib.parse.urlencode({
        'client_id': settings.GOOGLE_CLIENT_ID,
        'response_type': 'code',
        'state': state,
        'scope': ' '.join(scopes),
        'redirect_uri': settings.GOOGLE_REDIRECT_URI,
        'access_type': 'offline'
    })

    url = 'https://accounts.google.com/o/oauth2/v2/auth?{}'.format(options)

    return redirect(url)

def callback(request):
    if request.method != 'GET':
        return HttpResponse(status=405)

    if 'error' in request.GET:
        return HttpResponse(request.GET['error'], status=400)

    if 'state' not in request.GET or 'state' not in request.session:
        return HttpResponse('Did you forget something ?', status=400)

    if request.GET['state'] != request.session['state']:
        return HttpResponse('Now why would you do that ?', status=400)

    del request.session['state']

    options = {
        'code': request.GET['code'],
        'client_id': settings.GOOGLE_CLIENT_ID,
        'client_secret': settings.GOOGLE_CLIENT_SECRET,
        'redirect_uri': settings.GOOGLE_REDIRECT_URI,
        'grant_type': 'authorization_code',
    }

    try:
        with urllib.request.urlopen(
            'https://www.googleapis.com/oauth2/v4/token',
            urllib.parse.urlencode(options).encode()) as res:
                creds = json.loads(res.read().decode())
    except urllib.error.URLError as e:
        return HttpResponse(e.reason, status=500)

    try:
        headers = {
            'Authorization': 'Bearer ' + creds['access_token']
        }

        req = urllib.request.Request(
            url='https://www.googleapis.com/oauth2/v3/userinfo',
            headers = headers
        )

        with urllib.request.urlopen(req) as res:
            user_info = json.loads(res.read().decode())
    except urllib.error.URLError as e:
        return HttpResponse('Can not get email', status=e.code)

    request.session['username'] = user_info['email']
    request.session['access_token'] = creds['access_token']

    if 'refresh_token' in creds:
        request.session['refresh_token'] = creds['refresh_token']

    request.session['expires_in'] = time.time() + creds['expires_in']

    try:
        user = Users.objects.get(pk=user_info['email'])
        user.access_token = creds['access_token']
        user.expires_in = request.session['expires_in']
        request.session['refresh_token'] = user.refresh_token
    except Users.DoesNotExist:
        user = Users(
            username=user_info['email'],
            access_token=creds['access_token'],
            refresh_token=request.session['refresh_token'],
            expires_in=request.session['expires_in']
        )

    user.save()

    return redirect('home')

def logout(request):
    try:
        del request.session['username']
        del request.session['access_token']
        del request.session['refresh_token']
        del request.session['expires_in']
        request.session.flush()
    except KeyError:
        return HttpResponse('Logout failed', status=500)

    return redirect('home')

def remove(request):
    if 'username' not in request.session:
        return render(request, 'calman/login.html')

    if request.method != 'POST':
        return HttpResponse(status=405)

    req_body = json.loads(request.body.decode())

    headers = {
        'Authorization': 'Bearer ' + request.session['access_token']
    }

    for event_id in req_body['events']:
        url = 'https://www.googleapis.com/calendar/v3/calendars/primary/events/' + event_id

        req = urllib.request.Request(url, headers=headers, method='DELETE')

        try:
            urllib.request.urlopen(req)

            Events.objects.filter(event_id=event_id).delete()
        except urllib.error.URLError as e:
            return HttpResponse('Can not remove event', status=e.code)

    return HttpResponse()

def add(request):
    req_body = json.loads(request.body.decode())

    start_date = list(map(lambda x: int(x), req_body['start'].split('-')))
    end_date = list(map(lambda x: int(x), req_body['end'].split('-')))

    to_cal = json.dumps({
        'summary': req_body['title'],
        'description': req_body['description'],
        'start': {
            'dateTime': datetime.datetime(start_date[0], start_date[1], start_date[2]).isoformat(),
            'timeZone': settings.TIME_ZONE
        },
        'end': {
            'dateTime': datetime.datetime(end_date[0], end_date[1], end_date[2]).isoformat(),
            'timeZone': settings.TIME_ZONE
        }
    }).encode('utf-8')

    headers = {
        'Authorization': 'Bearer ' + request.session['access_token'],
        'Content-Type': 'application/json',
        'Content-Length': len(to_cal)
    }

    req = urllib.request.Request(
        url='https://www.googleapis.com/calendar/v3/calendars/primary/events',
        data=to_cal,
        headers=headers
    )

    try:
        with urllib.request.urlopen(req) as res:
            new_event = json.loads(res.read().decode())

            event = Events()
            event.event_id = new_event['id']
            event.username = Users.objects.get(pk=request.session['username'])
            event.title = req_body['title']
            event.description = req_body['description']
            event.start = req_body['start']
            event.end = req_body['end']

            event.save()
    except urllib.error.URLError as e:
        return HttpResponse('Can not add event', status=e.code)

    return render(request, 'calman/event.html', prep_res([event]))
