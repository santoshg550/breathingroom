from django.urls import path

from . import views

urlpatterns = [
    path('', views.home, name='home'),
    path('login/', views.login, name='login'),
    path('logout/', views.logout, name='logout'),
    path('callback/', views.callback, name='oauth_callback'),
    path('events/add/', views.add, name='add_events'),
    path('events/remove/', views.remove, name='remove_events'),
]
