from django.db import models

class Users(models.Model):
    username = models.CharField(primary_key=True, max_length=254)
    access_token = models.TextField()
    refresh_token = models.TextField()
    expires_in = models.IntegerField()

    def __str__(self):
        return self.username

class Events(models.Model):
    event_id = models.CharField(primary_key=True, max_length=255)
    username = models.ForeignKey(Users, on_delete=models.CASCADE)
    title = models.CharField(max_length=255)
    description = models.CharField(max_length=255)
    start = models.CharField(max_length=25)
    end = models.CharField(max_length=25)

    def __str__(self):
        return self.title + ' by ' + self.username
