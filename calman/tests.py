from django.test import TestCase, LiveServerTestCase
from django.urls import reverse
from django.conf import settings

from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as ec

import time
import urllib
import json

from .models import Users, Events

class UsersModelTests(TestCase):
    def test_login(self):
        res = self.client.get(reverse('login'))

        self.assertEqual(res.status_code, 302)

    def test_callback(self):
        res = self.client.get(reverse('oauth_callback') + '?state=abc')

        self.assertEqual(res.status_code, 400)

class OauthTests(LiveServerTestCase):
    def setUp(self):
        self.driver = webdriver.Chrome('/home/santosh/web_drivers/chrome_76/chromedriver')
        super(OauthTests, self).setUp()

    def tearDown(self):
        self.driver.quit()
        super(OauthTests, self).tearDown()

    def test_first_time_oauth_login(self):
        driver = self.driver

        res = self.client.get(reverse('login'))

        self.assertEqual(res.status_code, 302)

        self.assertEqual(res.has_header('location'), True)
        redirect_to = res.get('location')

        driver.get(redirect_to)

        wait = WebDriverWait(driver, 10)

        email_input = driver.find_element(By.CSS_SELECTOR, 'input[type=email]')
        if email_input:
            email_input.send_keys(settings.EMAIL_ID)
            email_input.send_keys(Keys.RETURN)

            password = wait.until(
                ec.visibility_of_element_located(
                    (By.CSS_SELECTOR, 'input[type=password]')
                )
            )
            password.send_keys(settings.EMAIL_PASSWORD)
            password.send_keys(Keys.RETURN)

            driver.implicitly_wait(10)

            try:
                gotoapp = driver.find_element(By.LINK_TEXT, 'Advanced')
                if gotoapp:
                    gotoapp.click()

                    gotoapp = driver.find_element(By.LINK_TEXT, 'Go to CalMan (unsafe)')
                    gotoapp.click()
            except:
                pass

        # If access is already granted, 'Allow' page will not show up.
        try:
            permit = wait.until(
                ec.visibility_of_element_located(
                    (By.ID, 'submit_approve_access')
                )
            )
            permit.click()
        except:
            pass

        time.sleep(5)

        uc = urllib.parse.urlparse(driver.current_url)

        res = self.client.get(uc.path + '?' + uc.query)

        res = self.client.get(res.get('location'))

        event = {
            'title': 'abc',
            'description': 'hello',
            'start': '2019-10-12',
            'end': '2019-11-12'
        }

        res = self.client.post(reverse('add_events'),
                                json.dumps(event),
                                content_type='application/json')

        self.assertEqual(res.status_code, 200)

        event = Events.objects.get(title='abc')
        self.assertEqual(event.title, 'abc')

        events = {
            'events': [
                event.event_id
            ]
        }
        res = self.client.post(reverse('remove_events'),
                                json.dumps(events),
                                content_type='application/json')

        try:
            event = Events.objects.get(title='abc')
        except Exception as e:
            self.assertEqual(True, True)
        else:
            self.assertEqual(False, True)

# To run tests specific to an app:
# python manage.py test appname
